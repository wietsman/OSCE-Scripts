https://www.abatchy.com/2017/03/osce-study-plan#osce-course-outline

#jmp instruction
https://thestarman.pcministry.com/asm/2bytejumps.htm
https://buffered.io/posts/jumping-with-bad-chars/

#Custom encoders
http://www.greycel.com/2017/04/16/Custom-Shellcode-Encoder/

#Cheatsheets
http://www.greycel.com/2017/04/15/Handy-Commands-ExpDev/

#
https://www.corelan.be/index.php/2011/11/18/wow64-egghunter/
https://www.securitysift.com/windows-exploit-development-part-4-locating-shellcode-jumps/
http://shell-storm.org/shellcode/
http://www.fuzzysecurity.com/tutorials.html
https://dilsec.com/2017/05/15/disk-savvy-enterprise-v9-6-18-buffer-overflow-seh-win-7-x64-exploit-development/


#Create shell code
http://www.gosecure.it/blog/art/452/sec/create-a-custom-shellcode-using-system-function/
https://chesteroni.blogspot.com
http://www.vividmachines.com/shellcode/shellcode.html
https://www.commandlinefu.com/commands/view/6051/get-all-shellcode-on-binary-file-from-objdump


#Lessons
https://tulpa-security.com/2017/07/18/288/

https://www.tutorialspoint.com/assembly_programming/assembly_registers.htm

# Notes
https://github.com/tsondt/osce

# Applications to hack
https://github.com/thelastbyte/OSCE
https://github.com/neb2886/OSCE-prep
https://github.com/73696e65/windows-exploits
https://github.com/ihack4falafel/OSCE


#Mona.py
https://www.corelan.be/index.php/2011/07/14/mona-py-the-manual/

